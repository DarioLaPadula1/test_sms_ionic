import { Component } from '@angular/core';
declare var SMSReceive: any;


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

    public buttonColor: string = "medium";

    constructor() { }

    async readSMS() {
        SMSReceive.startWatch(
            () => {
                document.addEventListener('onSMSArrive', (e: any) => {
                    var IncomingSMS = e.data;
                    this.processSMS(IncomingSMS);
                });
            },
            () => { alert('Errore'); }
        );
    }

    stop() {
        SMSReceive.stopWatch(
            () => { console.log('stop'); },
            () => { alert('Errore'); }
        );
    }

    processSMS(data) {
        var message = data.body.toLowerCase();

        if (message.includes('blue')) {
            this.buttonColor = 'primary';
        }
        else if (message.includes('red')) {
            this.buttonColor = 'danger';
        }
        else if (message.includes('green')) {
            this.buttonColor = 'success';
        }
        else {
            this.buttonColor = 'medium';
        }

        this.stop();
    }
}
