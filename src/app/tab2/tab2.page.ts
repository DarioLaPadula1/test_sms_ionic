import {Component} from '@angular/core';
import {SMS} from '@ionic-native/sms/ngx';
import {NavController} from '@ionic/angular';
import {AndroidPermissions} from '@ionic-native/android-permissions/ngx';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

    phone: number;
    text: string;

    constructor(private sms: SMS, public navController: NavController, private androidPermissions: AndroidPermissions) {
    }

    async checkAndSendSMS() {
        await

        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS).then(
            result => this.send(),
            err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.SEND_SMS)
        );
    }

    send() {
        var options = {
            replaceLineBreaks: false,
            android: {
                intent: ''
            }
        };

        this.sms.send(String(this.phone), this.text, options).then((result) => {
            alert(result);
        }).catch((error) => {
            alert(error);
        });
    }

}
